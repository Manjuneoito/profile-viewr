import Loginpage from "./pages/Loginpage";
import Signuppage from "./pages/Signuppage";
import Mainpage from "./pages/Mainpage";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
function App() {
  return (
    <div>
      <Router>
        <Switch>
          <Route path="/" exact>
            <Loginpage />
          </Route>
          <Route path="/Signuppage">
            <Signuppage />
          </Route>
          <Route path="/MainPage">
            <Mainpage />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
