import React from "react";
import { useState } from "react";
import TodoTotalList from "./TodoTotalList";

export function Todolist() {
  const [toDos, setToDos] = useState([]);
  const [toDo, setToDo] = useState("");
  const addTodo = () => {
    if (toDo === "") {
      alert("Enter An Item");
      return 0;
    }
    setToDos([{ id: Date.now(), text: toDo, status: true },...toDos,]);
    setToDo("");
  };
  const activate = (obj) => {
    setToDos(
      toDos.filter((obj2) => {
        if (obj2.id === obj.id) {
          obj2.status = !obj2.status;
        }
        return obj2;
      })
    );
  };
  const deleted = (obj) => {
    setToDos(toDos.filter((obj2) => obj2.id != obj.id));
  };

  return (
    <>
      <div className="bg-purple-400 h-screen w-5/6">
        <h1 className="text-center flex place-content-center pt-3 text-white font-extrabold text-5xl">
          <u>To Do App</u>
        </h1>
        <br />
        <div className="flex justify-center">
          <input
            value={toDo}
            onChange={(e) => setToDo(e.target.value)}
            type="text"
            placeholder="Enter a Text"
            className="w-1/4 h-12"
          />
          <button
            onClick={addTodo}
            className="m-2 b-2 rounded w-24 bg-green-400"
          >
            ADD
          </button>
        </div>
        <br />
        <div>
          <h1 className="text-4xl text-white flex justify-center">
            List Items
          </h1>
        </div>
        <TodoTotalList toDos={toDos} activate={activate} deleted={deleted} />
        <br/>
        <div className="flex justify-evenly">
          <div className="flex flex-col">
        <h1 className="text-2xl text-white">Active List</h1>
        {toDos
          .filter((value) => value.status)
          .map((item, index) => (
            <h1 className="text-xl text-yellow-500">{item.text}</h1>
          ))}
          </div>
          <div className="flex flex-col">
        <h1 className="text-2xl text-white" >Inactive List</h1>
        {toDos
          .filter((value) => !value.status)
          .map((item, index) => (
            <h1 className=" text-xl text-red-500">{item.text}</h1>
          ))}
          </div>
          </div>
      </div>
    </>
  );
}
