import React from 'react'
import {useHistory} from "react-router-dom"
function TopNav() {
    const history=useHistory()
    const logout = () => {
        history.push("/")
    }
    return (
        <div className="flex justify-between bg-red-200 p-8">
           <h1 className="text-2xl font-extrabold">LOGO</h1>
           <button onClick={logout} className="border-2 rounded-sm p-2 text-white bg-blue-400 hover:bg-blue-600">LogOut</button>
        </div>
    )
}

export default TopNav
 
