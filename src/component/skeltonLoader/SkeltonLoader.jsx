import React from "react";
import Skeleton from "react-loading-skeleton";

const SkeltonLoader = ({ count, width, height }) => {
  
  return (
    <>
      <Skeleton count={count} width={width} height={height} />
    </>
  );
};

export default SkeltonLoader;
