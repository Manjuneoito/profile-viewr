import React from "react";

const TodoTotalList = ({toDos,activate,deleted}) => {
  return (
    <>
      {toDos.map((obj) => {
        return (
          <>
            <div className="flex justify-center">
              <div className=" flex justify-evenly bg-green-200 p-2 w-1/2 m-2">
                <h1 className={obj.status ? " text-xl text-blue-900":" text-xl text-red-700 line-through"}> {obj.text}</h1>
                <button
                  onClick={() => {
                    activate(obj);
                  }}
                  value={obj.status}
                  className="bg-blue-500 p-2 w-24 rounded text-white hover:bg-green-500"
                >
                  {obj.status? "Deactivate" : "Activate"}
                </button>
                <button
                  onClick={(e) => {
                    deleted(obj);
                  }}
                  className="bg-blue-500 p-2 w-24 rounded text-white hover:bg-red-500"
                >
                  Delete
                </button>
              </div>
            </div>
          </>
        );
      })}
    </>
  );
};

export default TodoTotalList;
