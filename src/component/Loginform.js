import React from 'react'
import {useHistory} from "react-router-dom"

function Loginform() {
    const history=useHistory()
    const click = () => {
        history.push("/Signuppage")
    }
    const data=useHistory()
    const dashbord = () => {
        history.push("/Mainpage/home")
    }
    //Heloo 
    //Hello Manju
    return (
        <div className="text-center">
           <form>
                    <div>
                        
                        <input className="border-2 border-green-400 w-full p-1"
                            type='email'
                            id='email'
                            placeholder='Enter Your Email'
                        />
                    </div>
                    <br></br>
                    <div>
                        
                        <input className="border-2 border-green-400 w-full p-1"
                            type='password'
                            id='password'
                            placeholder='Enter Your Password'
                        />
                    </div>
                    <br></br>

                    <div>
                        <button onClick={dashbord} className="border-2 rounded border-green-400 bg-green-400 px-8 py-1 hover:bg-green-700 hover:border-green-700">Login</button>
                    </div>
                    <br/>
                    <p>Create New Account?<a  onClick={click} className="text-blue-500 cursor-pointer"><u>SignUp</u></a></p>
                </form>
           
        </div>
    )
}

export default Loginform
