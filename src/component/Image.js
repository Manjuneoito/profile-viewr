import React from 'react'
import image from "../asset/image.jpg"
function Image() {
    return (
        <div className="ml-20 flex justify-center items-center">
            <img src={image}/>
        </div>
    )
}

export default Image
