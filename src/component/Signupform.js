import React from 'react'
import {useHistory} from "react-router-dom"
function Signupform() {
    const history=useHistory()
    const event = () => {
        history.push("/")
    }
    return (
        <div className="text-center">
           <form>
                    <div>
                        
                        <input className="border-2 border-green-400 w-full p-1"
                            type='name'
                            id='name'
                            placeholder='Enter Your Name'
                        />
                    </div>
                    <br></br>
                    <div>
                        
                        <input className="border-2 border-green-400 w-full p-1"
                            type='name'
                            id='name'
                            placeholder='Enter Your City'
                        />
                    </div>
                    <br></br>
                    <div>
                        
                        <input className="border-2 border-green-400 w-full p-1"
                            type='email'
                            id='email'
                            placeholder='Enter Your Email'
                        />
                    </div>
                    <br></br>
                    <div>
                        
                        <input className="border-2 border-green-400 w-full p-1"
                            type='password'
                            id='password'
                            placeholder='Enter Your Password'
                        />
                    </div>
                    <br></br>

                    <div>
                        <button onClick={event} className="border-2 rounded border-green-400 bg-green-400 px-8 py-1 hover:bg-green-700 hover:border-green-700">SignUp</button>
                    </div>
                    <br/>
                    <p>Already Have An Account?<a  onClick={event} className="text-blue-500 cursor-pointer"><u>Login</u></a></p>
                </form>
           
        </div>
    )
}

export default Signupform
