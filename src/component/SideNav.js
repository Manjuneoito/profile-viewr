import React from 'react'
import { NavLink } from 'react-router-dom'




function SideNav() {
    
    return (
        <div className="bg-green-100 h-screen w-64 flex flex-col ">
            
            <NavLink to="/Mainpage/home" activeClassName="bg-yellow-500" className="p-3 text-red-700 hover:bg-blue-400">Home</NavLink>
            <NavLink to="/Mainpage/about" activeClassName="bg-yellow-500" className="p-3 text-red-700 hover:bg-blue-400 ">About Us</NavLink>
            <NavLink to="/Mainpage/todolist" activeClassName="bg-yellow-500" className="p-3 text-red-700 hover:bg-blue-400 ">To Do List</NavLink>
            
        </div>
    )
}

export default SideNav
