import React, { useState } from "react";
import Display from "./Display";

function Content() {
  const [state, setState] = useState(true);
  const changing = () => {
    setState(!state);
  };
  return (
    <div className="bg-purple-400 h-screen w-full">
      <h1 className="text-center flex place-content-center pt-64 text-white font-extrabold text-5xl">
        WelCome To My First React Web Page
      </h1>
      {state ? <Display /> : ""}
      <button
        onClick={changing}
        className="b-2 bg-green-500 text-white p-4 rouded flex flex-center ml-16"
      >
        Click Here
      </button>
    </div>
  );
}

export default Content;
