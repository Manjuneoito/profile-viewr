import React from "react";
import { useState } from "react";
import { json } from "../constants/Profile";

function Content1() {
  const [value, setValue] = useState();
  console.log(value);
  return (
    <>
      <div className="bg-purple-400 h-screen w-5/6">
        <h1 className="text-center flex place-content-center pt-3 text-white font-extrabold text-5xl">
          Profile Page
        </h1>
        <div className="flex justify-evenly m-8">
          <div className="">
            {json.map((obj, index) => (
              <div
                className="cursor-pointer text-xl hover:text-white"
                onClick={() => setValue(obj)}
              >
               <button  className="px-4 py-2 rounded-md text-sm font-medium border-2 border-green-700 m-2 focus:outline-none focus:ring transition text-white bg-green-500 hover:text-white hover:bg-green-700 hover:border-green-500  hover:shadow-xl active:bg-green-200 focus:ring-green-300" type="submit">{obj.name}</button>
              </div>
            ))}
          </div>
          <div className="">
            {value ? (
              <div className="bg-yellow-600 p-4 hover:bg-green-400 ">
                <img src={value.image} className="w-64" alt="picture"></img>
                <div className="text-2xl">{value.name}</div>

                <div>Age: {value.age}</div>

                <div>Mobile: {value.num}</div>

                <div>Location: {value.location}</div>
              </div>
            ) : (
              <div className="bg-yellow-600 p-4 hover:bg-green-400 ">
                <img
                  src={
                    "https://st4.depositphotos.com/4329009/19956/v/600/depositphotos_199564354-stock-illustration-creative-vector-illustration-default-avatar.jpg"
                  }
                  className="w-64"
                  alt="pic"
                ></img>
                <h1>Name:</h1>
                <h1>Age:</h1>
                <h1>Mobile:</h1>
                <h1>Location:</h1>
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  );
}
export default Content1;
