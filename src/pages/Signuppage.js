import React from 'react'
import Signupform from '../component/Signupform'
import Image from '../component/Image'

function Signuppage() {
    return (
        <div className="w-full h-screen bg-yellow-200 flex">
        <div className="w-1/2 flex justify-center items-center ">
            <Image />
        </div>
        <div className="w-1/2 flex justify-center items-center">
        <Signupform />
        </div>
        
       
        
    </div>
    )
}

export default Signuppage
