import React, { Suspense } from "react";

import TopNav from "../component/TopNav";
import SideNav from "../component/SideNav";

import { Route, Switch } from "react-router-dom";
import SkeltonLoader from "../component/skeltonLoader/SkeltonLoader";
import { Todolist } from "../component/Todolist";
const Content = React.lazy(() => import("../component/Content"));
const Content1 = React.lazy(() => import("../component/Content1"));
function Mainpage() {
  return (
    <div>
      <TopNav />
      <div className="flex w-full">
        <SideNav />
        <Switch>
          <Route path="/Mainpage/about">
          <Suspense fallback={<SkeltonLoader count={10} width={500} height={30} />}>
            <Content1 />
            </Suspense>
          </Route>
          <Route path="/Mainpage/todolist">
          <Suspense fallback={<SkeltonLoader count={10} width={500} height={30} />}>
            <Todolist />
            </Suspense>
          </Route>
          <Route path="/MainPage/home">
            <Suspense fallback={<SkeltonLoader count={10} width={500} height={30} />}>
               <Content /> 
              
            </Suspense>
          </Route>
        </Switch>
      </div>
    </div>
  );
}

export default Mainpage;
