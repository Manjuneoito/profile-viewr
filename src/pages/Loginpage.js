import React from 'react'
import Loginform from '../component/Loginform'
import Image from '../component/Image'

function Loginpage() {
   
    return (
        <div className="w-full h-screen bg-pink-200 flex">
            <div className="w-1/2 flex justify-center items-center ">
                <Image />
            </div>
            <div className="w-1/2 flex justify-center items-center">
            <Loginform />
            </div>
            
           
            
        </div>
    )
}

export default Loginpage
